//note: DECLARACIÓN DE OBJETOS Y VARIABLES.
var button = document.getElementById("button");
var button2 = document.getElementById("button2");

var numero = document.getElementById("numero");

var hello = document.getElementById("hi");

var date = new Date();
var hour = date.getHours(); /* note: La variable ".getHours()" muestra la hora que a su vez es tomado por la variable "Date" */
var day = date.getDay();

var bisiesto = document.getElementById("bisiesto");

var btnReloj = document.getElementById("reloj");
var detenerReloj = document.getElementById("detener-reloj");
var muestroHora = document.getElementById("muestro-hora");

var btnAlarma = document.getElementById("alarma");
var detenerAlarma = document.getElementById("detener-alarma");

//note: DECLARACIÓN DE FUNCIONES.
function eventClick(event) {
  // alert("Has presionado el botón."+event);
  alert("Has presionado el botón en el evento '" + event.type + "' con el objeto de nombre id '" + event.target.id + "'");
  console.log(event);

  event.target.style.borderRadius = "1em";
  event.target.style.fontsize = "2em";

  button2.removeEventListener("click", eventClick);
  button2.addEventListener("dblclick", otherEventClick);
}

function otherEventClick(event) {
  alert("Has presionado el botón en el evento '" + event.type + "' con el objeto de nombre id '" + event.target.id + "'");
  console.log(event);

  event.target.style.background = "black";
  event.target.style.color = "white";
}

function parImpar() {
  var numero = prompt("Ingresa un número");

  //note: isNaN is not a Number | true si el valor es alfanumérico, false si es numérico.
  if (isNaN(numero)) {
    alert("No me engañes eso no es un número.");
  }
  else {
    // alert("Eso es un número");
    var modulo = numero % 2;
    var tipo = (modulo == 0) ? "Par" : "Impar";
    alert("El número " + numero + " es " + tipo);
  }
}

function sayHello() {
  // alert("I'm alive!");
  // alert(date);
  // alert(hour);

  /* note: Algoritmo para saludar:
  1.- El día tiene 24hrs. que van de las 0 a las 23
  2.- Decimos "Deja dormir" de las 0 a las 5
  3.- Decimos "Buenos días" de las 6 a las 11
  4.- Decimos "Buenas tardes" de las 12 a las 18
  5.- Decimos "Buenas noches" de las 19 a las 23 */

  var sheetCSS = document.createElement("link");
  sheetCSS.rel = "stylesheet";

  // if (hour < 6)
  if (hour < 6) {
    alert("Deja dormir");
    sheetCSS.href = "./Assets/web05/activos/duermete.css";
  }
  // else if(hora > 5 && hora < 12) 
  else if (hour >= 6 && hour <= 11) {
    alert("Buenos días");
    sheetCSS.href = "./Assets/web05/activos/dia.css";
  }
  // else if(hora > 11 && hora < 19) 
  else if (hour >= 12 && hour <= 18) {
    alert("Buenas tardes");
    sheetCSS.href = "./Assets/web05/activos/tarde.css";
  }
  // else if(hora > 18 && hora < 24) 
  else if (hour >= 19 && hour <= 23) {
    alert("Buenas noches");
    sheetCSS.href = "./Assets/web05/activos/noche.css";
  }

  document.head.appendChild(sheetCSS);

  // alert(day);
  // note: Dom = 0, Lun = 1, Mar = 2, Miér = 3, Jue = 4, Vie = 5, Sáb = 6.

  switch (day) {
    case 0:
      alert("Es Domingo");
      break;

    case 4:
      alert("Es Jueves");
      break;

    case 5:
      alert("¡Por fin es Viernes!");
      break;

    case 6:
      alert("Es Sábado");
      break;

    default:
      alert("Estoy esperando el fin de semana.")
      break;
  }
}

function anioBisiesto() {
  var anio = prompt("Ingresa un año");

  if (isNaN(anio)) {
    alert("No me engañes eso no es un año.")
  } else {
    /* important: 1.- El año debe ser divisible entre 4, por ejemplo 2004, 2008. (Entonces el residuo es igual a 0) code = anio%4 == 0
  2.- Pero si el año es divisible entre 4 y 100, entonces no es bisiesto, por ejemplo 2100, 2200. code = anio%4 == 0 && anio%100 != 0 / el != indica que debe ser "diferente de"
  3.- Pero si el año es divisible entre 100 y 400, entonces si es bisiesto, por ejemplo 2000 y 2400. code = || anio%400 == 0 */
    if ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0) {
      alert("El año " + anio + " SÍ es bisiesto");
    }
    else {
      alert("El año " + anio + " NO es bisiesto");
    }
  }
}

function reloj() {
  var dateReloj = new Date();
  var hourReloj = dateReloj.getHours();
  var minuteReloj = dateReloj.getMinutes();
  var secondReloj = dateReloj.getSeconds();

  if (hourReloj <= 9) {
    hourReloj = "0" + hourReloj;
  }

  if (minuteReloj <= 9) {
    minuteReloj = "0" + minuteReloj;
  }

  if (secondReloj <= 9) {
    secondReloj = "0" + secondReloj;
  }

  muestroHora.innerHTML = "<h1>" + hourReloj + ":" + minuteReloj + ":" + secondReloj + "</h1>";
}

function alarma() {
  var audio = document.createElement("audio");
  audio.src = "./Assets/web05/activos/alarma.mp3";
  return audio.play();
}

/*note: ASIGNACIÓN DE EVENTOS.
Los manejadores de eventos semánticos se ejecutan a la carga del documento. */
window.onload = function () {
  button.onclick = eventClick;
  button.onclick = otherEventClick;

  button2.addEventListener("click", eventClick);

  numero.addEventListener("click", parImpar);

  hi.addEventListener("click", sayHello);

  bisiesto.addEventListener("click", anioBisiesto);

  btnReloj.addEventListener("click", function () {
    // setInterval(reloj, 1000);
    iniciarReloj = setInterval(reloj,1000);
  });

  detenerReloj.addEventListener("click", function () {
    clearInterval(iniciarReloj);
  });

  btnAlarma.addEventListener("click", function () {
    iniciarAlarma = setTimeout(alarma,3000);
  });

  detenerAlarma.addEventListener("click", function () {
    clearTimeout(iniciarAlarma);
  });
}