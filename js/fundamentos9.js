// note: Clases
var Animal = function (t) {
  var o = this;
  o.tipo = t;

  o.saluda = function (s) {
    alert(s);
    var audio = document.createElement("audio");
    audio.src = "./Assets/web05/activos/" + s + ".mp3";
    return audio.play();
  }
}

var Perro = function (n) {
  var o = this;
  o.nombre = n;
}

var Gato = function (n) {
  var o = this;
  o.nombre = n;
}

// note: Con ayuda del prototipo de JavaScript vamos a extender las características de mi clase Animal
Animal.prototype.domestico = false;

Animal.prototype.aparecer = function (n) {
  var imagen = document.createElement("img");
  imagen.src = "./Assets/web05/activos/" + n + ".jpg";
  imagen.id = n;
  document.body.appendChild(imagen);
  return imagen;
}

var oApolo = new Perro("Apolo");
console.log(oApolo);
// oApolo.saluda("guau");


// note: Herencia mediante el prototipo de JavaScript
Perro.prototype = new Animal("Mamífero");
Perro.prototype.domestico = true;

Gato.prototype = new Animal("Mamífero");
Gato.prototype.domestico = true;

var pApolo = new Perro("Apolo");
console.log(pApolo);
// pApolo.saluda("guau");
pApolo.aparecer("kenai");

var pFigaro = new Gato("Figaro");
console.log(pFigaro);
pFigaro.aparecer("mauricio")


window.addEventListener("load", function () {
  document.querySelector("#kenai").addEventListener("click", function () {
    pApolo.saluda("guau");
  });

  document.querySelector("#mauricio").addEventListener("click", function () {
    pFigaro.saluda("miau");
  })
});