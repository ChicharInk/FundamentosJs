//http://es.wikipedia.org/wiki/Tres_en_línea
// note: Arreglos
/* var miArreglo = new Array("Hola",19,true);
console.log(miArreglo);
console.log(miArreglo[0]);
console.log(miArreglo[1]);
console.log(miArreglo[2]); */

//note: DECLARACIÓN DE OBJETOS Y VARIABLES.
var turno = 1;
var queTurno;
var arregloGato = new Array(9);
var celdas = document.getElementsByClassName("gato");
/* todo: Estudiar el getElementsByTagName 
var etiqueta = document.getElementsByTagName("td");
console.log(etiqueta); */

//note: DECLARACIÓN DE FUNCIONES.
function ganaJugador(letra) {
  if (
      (arregloGato[0] == letra && arregloGato[1] == letra && arregloGato[2] == letra) ||
      (arregloGato[3] == letra && arregloGato[4] == letra && arregloGato[5] == letra) ||
      (arregloGato[6] == letra && arregloGato[7] == letra && arregloGato[8] == letra) ||
      (arregloGato[0] == letra && arregloGato[3] == letra && arregloGato[6] == letra) ||
      (arregloGato[1] == letra && arregloGato[4] == letra && arregloGato[7] == letra) ||
      (arregloGato[2] == letra && arregloGato[5] == letra && arregloGato[8] == letra) ||
      (arregloGato[0] == letra && arregloGato[4] == letra && arregloGato[8] == letra) ||
      (arregloGato[2] == letra && arregloGato[4] == letra && arregloGato[6] == letra)) {
    alert("¡Jugador " + letra + " Ganaste!")
    window.location.reload();
  }
}

function gato(evento) {
  // alert("I'm Alive!")
  // alert(evento.target.id);
  var celda = evento.target;
  celda.removeEventListener("click", gato);

  var idCelda = evento.target.id;
  // alert(idCelda);
  // alert(idCelda.length);
  // alert(idCelda[1]);

  var posicionAMarcar = idCelda[1] - 1;
  // alert(posicionAMarcar);

  queTurno = turno % 2;
    // Turno Impares "X"
  if (queTurno == 1) {
    celda.innerHTML = "X";
    celda.style.background = "#EC673A";
    arregloGato[posicionAMarcar] = "X";
    ganaJugador("X");
    // Turno Pares "O"
  // } else if (queTurno == 0) {
  } else {
    celda.innerHTML = "O";
    celda.style.background = "#1C5F81";
    celda.style.color = "#FFF";
    arregloGato[posicionAMarcar] = "O";
    ganaJugador("O");
  }

  // console.log(turno, queTurno, arregloGato);

  // console.log(turno, queTurno);

  if (turno == 9) {
    alert("Empate");
    // console.log(window.location);
    window.location.reload();
  } else {
    turno++;
  }
}

function cargarDocumento() {
  // document.getElementById("c1").addEventListener("click",gato);
  // console.log(document.getElementsByClassName("gato"));
  // document.getElementsByClassName("gato").addEventListener("click",gato);
  // document.getElementsByClassName("gato")[4].addEventListener("click",gato);
  var n = 0;

  while (n < celdas.length) {
    // console.log(n, celdas[n]);
    celdas[n].addEventListener("click", gato);
    n++;
  }
}



//note: ASIGNACIÓN DE EVENTOS.
window.addEventListener("load", cargarDocumento);