//note: DECLARACIÓN DE OBJETOS Y VARIABLES.
var down = document.querySelector("#down"),
  press = document.querySelector("#press"),
  up = document.querySelector("#up"),
  leftToRight = 0,
  topToBottom = 0,
  pagina = document.querySelector("#pagina"),
  pantalla = document.querySelector("#pantalla"),
  barra = document.querySelector("#barra"),
  subir = document.querySelector("#subir"),
  mapa = document.querySelector("#mapa"),
  iframe = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3762.515851321896!2d-99.13679208548787!3d19.433313745694463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1f92d557cb83f%3A0xab4327a51d168cd9!2sMuseo+Mexicano+del+Dise%C3%B1o!5e0!3m2!1ses-419!2smx!4v1516255027853" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';

//note: DECLARACIÓN DE FUNCIONES.
function moverObjeto(queTecla) {
  // console.log(queTecla);
  if (queTecla == 37) /* cursor izquierda */ {
    // caja.style.left = "-100px";
    leftToRight -= 10;
    caja.style.left = leftToRight + "px";
  } else if (queTecla == 38) /* cursor arriba */ {
    // caja.style.top = "-100px";
    topToBottom -= 10;
    caja.style.top = topToBottom + "px";
  } else if (queTecla == 39) /* cursor derecha */ {
    // caja.style.left = "100px";
    leftToRight += 10;
    caja.style.left = leftToRight + "px";
  } else if (queTecla == 40) /* cursor abajo */ {
    // caja.style.top = "100px";
    topToBottom += 10;
    caja.style.top = topToBottom + "px";
  } else if (queTecla == 82) /* tecla "r" redondear */ {
    caja.style.borderRadius = "100%";
  } else if (queTecla == 67) /* tecla "c" cuadrado */ {
    caja.style.borderRadius = "0";
  }
}

function teclado(evento) {
  // console.log("Parámetro", evento);
  evento = window.event;
  // console.log("Objeto", evento);
  if (evento.type == "keydown") {
    down.innerHTML = "keydown: " + String.fromCharCode(evento.keyCode) + "-" + evento.keyCode;
  } else if (evento.type == "keypress") {
    press.innerHTML = "keypress: " + String.fromCharCode(evento.keyCode) + "-" + evento.keyCode;
  } else if (evento.type == "keyup") {
    up.innerHTML = "keyup: " + String.fromCharCode(evento.keyCode) + "-" + evento.keyCode;
  }

  moverObjeto(evento.keyCode); {

  }

  function mouse(evento) {
    // console.log(evento);
    evento = window.event;

    pagina.innerHTML = "Coordenadas del Mouse en la Página: X - (" + evento.pageX + "), Y (" + evento.pageY + ").";
    pantalla.innerHTML = "Coordenadas del Mouse en la Página: X - (" + evento.screenX + "), Y (" + evento.screenY + ").";

  }
}

function barrasScroll(evento) {
  // console.log(evento);

  var barraV = document.documentElement.scrollTop,
    barraH = document.documentElement.scrollLeft;

  // console.log(barraH, barraV);

  if (barraV > 20) {
    subir.style.opacity = 1;
  } else if (barraH > 200) {
    subir.style.opacity = 1;
  } else {
    subir.style.opacity = 0;
  }
}

function mediaQueries() {
  var anchoPantalla = window.innerWidth,
    altoPantalla = window.innerHeight;

  // console.log(anchoPantalla, altoPantalla);
  
  if (anchoPantalla > 1024) {
    mapa.innerHTML = iframe;
  } else {
    mapa.innerHTML = null;
  }
}

function cargaDocumento() {
  document.addEventListener("keydown", teclado);
  document.addEventListener("keypress", teclado);
  document.addEventListener("keyup", teclado);

  document.addEventListener("mousemove", mouse);

  window.addEventListener("scroll", barrasScroll);

  subir.addEventListener("click", function () {
    document.documentElement.scrollTop = 0;
    document.documentElement.scrollLeft = 0;
  });
  // console.log(subir, barra);

  window.addEventListener("resize", mediaQueries);
}

//note: ASIGNACIÓN DE EVENTOS.
window.addEventListener("load", cargaDocumento);