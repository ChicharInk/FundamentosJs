# Programación en JavaScript

[Slides](http://bextlan.com/slides/poo-js)

[Fundamentos de Programación JavaScript 1](../FundamentosJS/fundamentos1.html)
En este ejercicio aprendimos:

    Comentarios
    Clases-Objetos
    Métodos-Funciones
    Atributos-Variables
    El contexto this
    Tipos de Datos: Strings, Numbers, Booleans
    Escribir en el documento, consola y alertas
    JavaScript acepta código HTML
    Objetos JavaScript: document,window,navigator
    Concatenación
    Operadores: Ternario, Asignación, Comparación Idéntico, Negociación
    Crear Elementos HTML y modificar su CSS con JS
    Ámbito de la variables
    Detectar e interpretar los errores de programación de JavaScript en la consola del navegador


[Fundamentos de Programación en JavaScript 2](../FundamentosJS/fundamentos2.html)
En este ejercicio aprendimos:

    Algoritmos y Diagramas de Flujo
    Eventos y Manejadores de Eventos
    Document Object Model
    Función isNaN
    Clase Date
    Condicionales (if...else)
    Condicionales (if...else if...else)
    Condicionales (switch-case)
    Operadores: Aritméticos, Relacionales, Lógicos
    Temporizadores


[Fundamentos de Programación en JavaScript 3](../FundamentosJS/fundamentos3.html)
En este ejercicio aprendimos:

    Juego del Gato (Tres en Raya)
    Ciclo Whiles
    Contadores
    Arreglos


[Fundamentos de Programación en JavaScript 4](../FundamentosJS/fundamentos4.html)
En este ejercicio aprendimos:

    Arreglos Múltiples
    Ciclo for


[Fundamentos de Programación en JavaScript 5](../FundamentosJS/fundamentos5.html)
En este ejercicio aprendimos:

    JSON


[Fundamentos de Programación en JavaScript 6](../FundamentosJS/fundamentos6.html)
En este ejercicio aprendimos:

    Trabajo con números
    Método querySelector
    Clase Matemáticas
    Redondeos y Números Aleatorios
    Trabajo con Cadenas de Texto
    Longitud de Caracteres, Mayúsculas y Minúsculas
    Secuencias de Escape
    Validación de Formularios
    Expresiones Regulares
    Prevención de Eventos
    Métodos para Cadenas de Texto


[Fundamentos de Programación en JavaScript 7](../FundamentosJS/fundamentos7.html)
En este ejercicio aprendimos:

    El objeto event
    Detección del Teclado
    Detección del Mouse
    Detección del Scroll
    Redimensionamiento de la Pantalla
    Principios de Responsive Design con JavaScript


[Fundamentos de Programación en JavaScript 8](../FundamentosJS/fundamentos8.html)
En este ejercicio aprendimos:

    Objeto Navigator
    User Agent
    Detección de Dispositivos, Plataformas y Navegadores


[Fundamentos de Programación en JavaScript 9](../FundamentosJS/fundamentos9.html)
En este ejercicio aprendimos:

    Objetos
    Prototipos
    Herencia


Fuentes de Consulta
Más sobre JavaScript:

[Introducción a JavaScript](http://librosweb.es/javascript)
[Introducción a AJAX](http://librosweb.es/ajax)
[Estándares Web](http://www.codexexemplaorg/curso/itdex.php)